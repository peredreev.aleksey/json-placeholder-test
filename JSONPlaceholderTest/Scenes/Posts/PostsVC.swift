//
//  PostsVC.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import UIKit

class PostsVC: UIViewController {
    
    // MARK: - Properties
    
    var viewModel: PostsViewModelProtocol?
    
    var posts: [ApiModels.Post?]?
    
    // MARK: - Outlets
    
    @IBOutlet weak var postsTableView: UITableView!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupData()
    }
    
    // MARK: - Methods

    func setupData() {
        self.postsTableView.delegate = self
        self.postsTableView.dataSource = self
        self.postsTableView.register(R.nib.postCell)
        
        self.viewModel = PostsViewModel()
        self.bindUI()
        self.viewModel?.getPosts()
        
    }
    
    func bindUI() {
        viewModel?.isLoaderHidden.bind({ [weak self] in
            self?.shouldHideLoader(isHidden: $0)
        })
        viewModel?.alertMessage.bind({ [weak self] in
            self?.showAlertMessage($0)
        })
        viewModel?.posts.bind({ [weak self] in
            self?.posts = $0
            DispatchQueue.main.async {
                self?.postsTableView.reloadData()
            }
        })
    }
    
}

extension PostsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.posts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = postsTableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.postCell, for: indexPath) else {
            fatalError("postCell not registered.")
        }
        
        if let post = self.posts?[indexPath.row] {
            cell.setup(post: post)
        }
    
        return cell
    }
}
