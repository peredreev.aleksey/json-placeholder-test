//
//  PostsViewModel.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import UIKit
import Alamofire

protocol PostsViewModelProtocol {
    var alertMessage: Dynamic<AlertMessage> { get set }
    var isLoaderHidden: Dynamic<Bool> { get set }
    
    var posts: DynamicArray<ApiModels.Post?> { get set }
    
    func getPosts()
}

class PostsViewModel: NSObject, PostsViewModelProtocol {
    
    // MARK: - PostsViewModelProtocol
    
    var alertMessage: Dynamic<AlertMessage> = Dynamic(AlertMessage(title: "", body: ""))
    var isLoaderHidden: Dynamic<Bool> = Dynamic(true)
    var posts: DynamicArray<ApiModels.Post?> = DynamicArray([])
    
    // MARK: - Properties
    
    private let apiManager = APIManager(sessionManager: Session(), retrier: APIManagerRetrier())
    
    // MARK: - Public methods
    
    func getPosts() {
        self.isLoaderHidden.value = false
        self.apiManager.call(type: RequestItemsType.getPosts) { (result: Swift.Result<[ApiModels.Post], AlertMessage>) in
            self.isLoaderHidden.value = true
            switch result {
            case .success(let posts):
                self.posts.value = posts
                break
            case .failure(let message):
                self.alertMessage.value = message
                break
            }
        }
    }
    
    // MARK: - Init
    
    override init() {
        super.init()
    }
    
}

