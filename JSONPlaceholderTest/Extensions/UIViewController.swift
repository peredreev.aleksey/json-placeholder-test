//
//  UIViewController.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import UIKit
import SVProgressHUD
import SwiftMessages

extension UIViewController {
    
    // MARK: - SVProgressHUD Methods
    
    func shouldHideLoader(isHidden: Bool) {
        if isHidden {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
        } else {
            SVProgressHUD.show()
        }
    }
    
    // MARK: - SwiftMessages Methods
    
    func showAlertMessage(_ message: AlertMessage) {
        let messageView: MessageView = MessageView.viewFromNib(layout: .centeredView)
        messageView.configureBackgroundView(width: 250)
        messageView.configureContent(title: message.title, body: message.body, iconImage: nil, iconText: "☹️", buttonImage: nil, buttonTitle: "ОК") { _ in
            SwiftMessages.hide()
        }
        messageView.backgroundView.backgroundColor = UIColor.init(white: 0.97, alpha: 1)
        messageView.backgroundView.layer.cornerRadius = 10
        var config = SwiftMessages.defaultConfig
        config.presentationStyle = .center
        config.duration = .forever
        config.dimMode = .blur(style: .dark, alpha: 1, interactive: true)
        config.presentationContext  = .window(windowLevel: UIWindow.Level.statusBar)
        SwiftMessages.show(config: config, view: messageView)

    }
    
}

