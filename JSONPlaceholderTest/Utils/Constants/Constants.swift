//
//  Constants.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Foundation

class Constants {

    // MARK: - Alerts
    
    static let defaultAlertTitle = "warning"
    static let errorAlertTitle = "error"
    static let genericErrorMessage = "Something went wrong, please try again."
    
}
