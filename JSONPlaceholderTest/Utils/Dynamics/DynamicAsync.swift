//
//  DynamicAsync.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Foundation

class DynamicAsync<T>: Dynamic<T> {
    
    // MARK: - Overrides
    
    override func fire() {
        -->{ self.listener?(self.value) }
    }
    
    // MARK: - Initialization
    
    override init(_ v: T) {
        super.init(v)
    }
}

