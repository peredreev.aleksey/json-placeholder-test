//
//  EndpointType.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Alamofire

protocol EndpointType {
    
    // MARK: - Properties
    
    var baseURL: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var url: URL { get }
    var encoding: ParameterEncoding { get }
    var version: String { get }
    
}
