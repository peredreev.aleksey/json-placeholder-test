//
//  ApiManager.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Alamofire

class APIManager {
    
    // MARK: - Properties
    
    private let sessionManager: Session
    private let retrier: APIManagerRetrier
    static  let networkEnviroment: NetworkEnvironment = .production
    
    // MARK: - Public methods
    
    func call(type: EndpointType, params: Parameters? = nil, handler: @escaping (Result<(), AlertMessage>) -> Void) {
        self.sessionManager.request(type.url,
                                    method: type.httpMethod,
                                    parameters: params,
                                    encoding: type.encoding,
                                    headers: type.headers).cURLDescription { description in
                                        print(description)
                                    }.validate().responseJSON { (data) in
                                        debugPrint(data)
                                        switch data.result {
                                        case .success(_):
                                            handler(.success(()))
                                            break
                                        case .failure(_):
                                            handler(.failure(self.parseApiError(data: data.data)))
                                            break
                                        }
                                    }
    }
    
    func call<T: Codable>(type: EndpointType, params: Parameters? = nil, handler: @escaping (Result<T, AlertMessage>) -> Void) {
        self.sessionManager.request(type.url,
                                    method: type.httpMethod,
                                    parameters: params,
                                    encoding: type.encoding,
                                    headers: type.headers).cURLDescription { description in
                                        print(description)
                                    }.validate().responseJSON { (data) in
                                        debugPrint(data)
                                        do {
                                            guard let jsonData = data.data else {
                                                throw AlertMessage(title: "Ошибка", body: "Нет данных")
                                            }
                                            let result = try JSONDecoder().decode(T.self, from: jsonData)
                                            handler(.success(result))
                                            self.resetNumberOfRetries()
                                        } catch {
                                            if let error = error as? AlertMessage {
                                                return handler(.failure(error))
                                            }
                                            
                                            handler(.failure(self.parseApiError(data: data.data)))
                                        }
                                    }
    }
    
    // MARK: - Private methods
    
    private func resetNumberOfRetries() {
        self.retrier.numberOfRetries = 0
    }
    
    private func parseApiError(data: Data?) -> AlertMessage {
        let decoder = JSONDecoder()
        if let jsonData = data, let error = try? decoder.decode(NetworkError.self, from: jsonData) {
            return AlertMessage(title: Constants.errorAlertTitle, body: error.key ?? error.message)
        }
        return AlertMessage(title: Constants.errorAlertTitle, body: Constants.genericErrorMessage)
    }
    
    // MARK: - Initialization
    
    init(sessionManager: Session, retrier: APIManagerRetrier) {
        self.sessionManager = sessionManager
        self.retrier = retrier
    }
    
}
