//
//  RequestItems.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Alamofire

// MARK: - Enums

enum NetworkEnvironment {
    case dev
    case production
}

enum RequestItemsType {
    
    // MARK: Posts
    
    case getPosts
    case postPosts
    case putPosts
    case patchPosts
    case deletePosts
    
    // MARK: Comments

    case getComments
    case postComments
    case putComments
    case patchComments
    case deleteComments

    // MARK: Albums

    case getAlbums
    case postAlbums
    case putAlbums
    case patchAlbums
    case deleteAlbums

    // MARK: Photos

    case getPhotos
    case postPhotos
    case putPhotos
    case patchPhotos
    case deletePhotos

    // MARK: ToDos

    case getToDos
    case postToDos
    case putToDos
    case patchToDos
    case deleteToDos

    // MARK: Users

    case getUsers
    case postUsers
    case putUsers
    case patchUsers
    case deleteUsers
    
}

// MARK: - Extensions
// MARK: - EndPointType

extension RequestItemsType: EndpointType {
    
    // MARK: - Properties
    
    var baseURL: String {
        switch APIManager.networkEnviroment {
            case .dev: return "https://jsonplaceholder.typicode.com/"
            case .production: return "https://jsonplaceholder.typicode.com/"
        }
    }
    
    var version: String {
        return "/v1"
    }
    
    var path: String {
        switch self {
        case .getPosts, .postPosts, .putPosts, .patchPosts, .deletePosts:
            return "posts"
        case .getComments, .postComments, .putComments, .patchComments, .deleteComments:
            return "comments"
        case .getAlbums, .postAlbums, .putAlbums, .patchAlbums, .deleteAlbums:
            return "albums"
        case .getPhotos, .postPhotos, .putPhotos, .patchPhotos, .deletePhotos:
            return "photos"
        case .getToDos, .postToDos, .putToDos, .patchToDos, .deleteToDos:
            return "todos"
        case .getUsers, .postUsers, .putUsers, .patchUsers, .deleteUsers:
            return "users"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .getPosts, .getComments, .getAlbums, .getPhotos, .getToDos, .getUsers:
            return .get
        case .postPosts, .postComments, .postAlbums, .postPhotos, .postToDos, .postUsers:
            return .post
        case .putPosts, .putComments, .putAlbums, .putPhotos, .putToDos, .putUsers:
            return .put
        case .patchPosts, .patchComments, .patchAlbums, .patchPhotos, .patchToDos, .patchUsers:
            return .patch
        case .deletePosts, .deleteComments, .deleteAlbums, .deletePhotos, .deleteToDos, .deleteUsers:
            return .delete
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        default:
            return ["Content-Type": "application/json",
                    "Charset": "UTF-8"]
        }
    }
    
    var url: URL {
        switch self {
        default:
            return URL(string: self.baseURL + self.path)!
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        default:
            return JSONEncoding.default
        }
    }
    
}
