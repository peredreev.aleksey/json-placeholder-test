//
//  ApiModels.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Foundation

enum ApiModels {
    
    struct Post: Codable {
        let userId: Int
        let id: Int
        let title: String
        let body: String
        
        enum CodingKeys: String, CodingKey {
            case userId = "userId"
            case id = "id"
            case title = "title"
            case body = "body"
        }
    }
    
}
