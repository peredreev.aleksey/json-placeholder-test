//
//  NetworkMessage.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Foundation

class NetworkError: Codable {
    
    let message: String
    let key: String?
    
}
