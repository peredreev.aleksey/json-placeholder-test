//
//  AlertMessage.swift
//  JSONPlaceholderTest
//
//  Created by Alex Peredreev on 09.07.2021.
//

import Foundation

class AlertMessage: Error {
    
    // MARK: - Properties
    
    var title = ""
    var body = ""
    
    // MARK: - Initialization
    
    init(title: String, body: String) {
        self.title = title
        self.body = body
    }
    
}

